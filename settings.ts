interface ComputeParams {
  start_y: number;
  end_y: number;
  settings: Settings;
}

interface Settings {
  width: number;
  height: number;
  target_width: number;
  target_height: number;
  infinity_bound: number;
  center_x: number;
  center_y: number;
}
