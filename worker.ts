self.onmessage = (e) => {
  let params = e.data as ComputeParams;
  let data = compute(params.start_y, params.end_y, params.settings);
  postMessage({ data, start_y: params.start_y, end_y: params.end_y });
};

function compute(start_y: number, end_y: number, settings: Settings) {
  let data_array = new Uint8ClampedArray(
    settings.target_width * (end_y - start_y) * 4
  );

  for (let x = 0; x < settings.target_width; ++x) {
    for (let y = start_y; y < end_y; ++y) {
      let data_y = y -start_y;
      let index = (data_y * settings.target_width + x) * 4;
      let l = get_level_at(x, y, settings);
      let level = Math.ceil((l * 255.0) / settings.infinity_bound);
      data_array[index + 0] = level;
      data_array[index + 1] = level;
      data_array[index + 2] = level;
      data_array[index + 3] = 255;
    }
  }
  return data_array;
}

function scale_x(x: number, settings: Settings) {
  return (x / settings.target_width) * settings.width;
}
function scale_y(y: number, settings: Settings) {
  return (y / settings.target_height) * settings.height;
}

function transform_x(x: number, settings: Settings) {
  return scale_x(x, settings) + settings.center_x - settings.width / 2.0;
}
function transform_y(y: number, settings: Settings) {
  return scale_y(y, settings) + settings.center_y - settings.width / 2.0;
}
function get_level_at(x: number, y: number, settings: Settings) {
  return get_level(
    transform_x(x, settings),
    transform_y(y, settings),
    settings
  );
}

function get_level(origin_a: number, origin_b: number, settings: Settings) {
  let a = origin_a;
  let b = origin_b;

  let n = 0;
  let aa: number;
  let bb: number;
  while (n < settings.infinity_bound && a * a + b * b < 4.0) {
    aa = a * a - b * b;
    bb = 2.0 * a * b;
    a = aa + origin_a;
    b = bb + origin_b;
    n += 1;
  }
  return n;
}
