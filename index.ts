const precision_slider = <HTMLInputElement>(
  document.getElementById("max_i_slider")
);
const high_res_button = <HTMLButtonElement>(
  document.getElementById("high_res_button")
);
const res_width_input = <HTMLButtonElement>document.getElementById("res_width");
const input_x = <HTMLButtonElement>document.getElementById("input_x");
const input_y = <HTMLButtonElement>document.getElementById("input_y");
const zoom_in_button = <HTMLInputElement>document.getElementById("zoom_in");
const zoom_out_button = <HTMLInputElement>document.getElementById("zoom_out");

const total_time = <HTMLSpanElement>document.getElementById("total_time");
const render_time = <HTMLSpanElement>document.getElementById("render_time");

const canvas = <HTMLCanvasElement>document.getElementById("canvas");
const ctx = <CanvasRenderingContext2D>canvas.getContext("2d");
// const WIDTH = window.innerWidth
// const HEIGHT = window.innerHeight
const WIDTH = 800;
const HEIGHT = 800;
canvas.width = WIDTH;
canvas.height = HEIGHT;
ctx.imageSmoothingEnabled = false;

precision_slider.addEventListener("change", (event) => {
  viewport.set_max_i(parseInt(precision_slider.value, 10));
  requestUpdate();
});

const workers: Worker[] = [];

const worker_count = 8;

for (let i = 0; i < worker_count; ++i) {
  const worker = new Worker("./worker.js");
  workers.push(worker);
}

let start = performance.now();

class Viewport {
  width: number;
  height: number;
  center_x: number;
  center_y: number;
  target_width: number;
  target_height: number;
  max_i: number;
  data_array: Uint8ClampedArray;
  step: number;
  recieved: number;
  waiting = false;
  constructor(
    width: number,
    height: number,
    center_x: number,
    center_y: number,
    target_width: number,
    target_height: number
  ) {
    this.width = width;
    this.height = height;
    this.center_x = center_x;
    this.center_y = center_y;
    this.target_height = target_height;
    this.target_width = target_width;
    this.data_array = new Uint8ClampedArray(target_width * target_height * 4);
    this.data_array.fill(255);
    this.max_i = 100;
    this.step = Math.floor(this.target_height / worker_count);
    this.recieved = 0;

    for (const worker of workers) {
      worker.onmessage = (event) => {
        const data = event.data.data;
        const start_y = event.data.start_y;
        this.data_array.set(data, 0);

        const img = new ImageData(data, WIDTH, this.step);
        ctx.putImageData(img, 0, start_y);


        this.onRecieve();
      };
    }
  }

  onRecieve() {
    this.recieved += 1;
    if (this.recieved == worker_count) {
      this.recieved = 0;
      const total = performance.now() - start;
      total_time.innerText = total.toFixed(1);
      this.waiting = false;
    }
  }

  scaleX(x: number) {
    return (x / WIDTH) * this.width + this.center_x;
  }
  scaleY(y: number) {
    return (y / HEIGHT) * this.height + this.center_y;
  }

  zoom(multiply: number) {
    if (multiply < 0.1) {
      multiply = 0.1;
    }
    this.width = this.width * (1.0 / multiply);
    this.height = this.height * (1.0 / multiply);
  }
  scale_x(x: number) {
    return (x / this.target_width) * this.width;
  }
  scale_y(y: number) {
    return (y / this.target_height) * this.height;
  }
  transform_x(x: number) {
    return this.scale_x(x) + this.center_x - this.width / 2.0;
  }
  transform_y(y: number) {
    return this.scale_y(y) + this.center_y - this.width / 2.0;
  }

  move_delta(delta_x: number, delta_y: number) {
    let dx = this.scale_x(delta_x);
    let dy = this.scale_y(delta_y);
    this.center_x += dx;
    this.center_y += dy;
  }

  get_x() {
    return this.center_x;
  }
  get_y() {
    return this.center_y;
  }
  set_x(x: number) {
    this.center_x = x;
  }
  set_y(y: number) {
    this.center_y = y;
  }

  request_draw() {
    if (this.waiting) {
      return;
    }

    this.waiting = true;
    let y = 0;

    for (let i = 0; i < worker_count; ++i) {
      let start_y = y;
      let end_y = y + this.step;
      if (i == worker_count - 1) {
        end_y = this.target_height;
      }
      let worker = workers[i];
      worker.postMessage({
        start_y: start_y,
        end_y: end_y,
        settings: {
          width: this.width,
          height: this.height,
          target_width: this.target_width,
          target_height: this.target_height,
          infinity_bound: this.max_i,
          center_x: this.center_x,
          center_y: this.center_y,
        },
      });
      y = end_y;
    }
  }
  get_array() {
    return this.data_array;
  }

  get_max_i() {
    return this.max_i;
  }
  set_max_i(n: number) {
    this.max_i = n;
  }
}

const viewportDivider = HEIGHT / 3;

let viewport = new Viewport(
  WIDTH / viewportDivider,
  HEIGHT / viewportDivider,
  -0.7,
  0,
  WIDTH,
  HEIGHT
);

// let viewport = MandelbrotViewport.new(WIDTH / viewportDivider, HEIGHT / viewportDivider, -0.7, 0, WIDTH, HEIGHT)

precision_slider.value = viewport.get_max_i().toString();

input_x.value = viewport.get_x().toString();
input_y.value = viewport.get_y().toString();

let zoom = 1;

canvas.addEventListener("wheel", (event) => {
  event.preventDefault();

  let delta = event.deltaY;
  zoom = zoom - (delta / HEIGHT) * 4;
  requestUpdate();
});

let startX = 0;
let startY = 0;

let mousedown = false;

canvas.addEventListener("mousedown", (event) => {
  startX = event.offsetX;
  startY = event.offsetY;
  mousedown = true;
});
canvas.addEventListener("mouseup", (event) => {
  mousedown = false;
});
canvas.addEventListener("mouseleave", (event) => {
  mousedown = false;
});

canvas.addEventListener("mousemove", (event) => {
  if (mousedown) {
    let deltaX = startX - event.offsetX;
    let deltaY = startY - event.offsetY;

    viewport.move_delta(deltaX, deltaY);
    startX = event.offsetX;
    startY = event.offsetY;
    input_x.value = viewport.get_x().toString();
    input_y.value = viewport.get_y().toString();
  }

  requestUpdate();
});

input_x.addEventListener("change", () => {
  viewport.set_x(parseFloat(input_x.value));
  requestUpdate();
});

input_y.addEventListener("change", () => {
  viewport.set_y(parseFloat(input_y.value));
  requestUpdate();
});

const zoom_in = () => {
  viewport.zoom(1.1);
  requestUpdate();
};
const zoom_out = () => {
  viewport.zoom(0.9);
  requestUpdate();
};

let zoom_interval_ms = 30;

let zoom_direction = 0;

const zoom_loop = () => {
  if (zoom_direction != 0) {
    viewport.zoom(1 + zoom_direction / 10);
    requestUpdate();
  }
};
setInterval(zoom_loop, zoom_interval_ms);

zoom_in_button.addEventListener("mousedown", () => {
  zoom_direction = +1;
});
zoom_in_button.addEventListener("mouseup", () => {
  zoom_direction = 0;
});
zoom_in_button.addEventListener("mouseleave", () => {
  zoom_direction = 0;
});
zoom_out_button.addEventListener("mousedown", () => {
  zoom_direction = -1;
});
zoom_out_button.addEventListener("mouseup", () => {
  zoom_direction = 0;
});
zoom_out_button.addEventListener("mouseleave", () => {
  zoom_direction = 0;
});
let updateRequested = false;

function requestUpdate() {
  if (!updateRequested) {
    updateRequested = true;
    requestAnimationFrame(draw);
  }
}

function draw() {
  start = performance.now();
  viewport.zoom(zoom);

  zoom = 1;
  viewport.request_draw();

  updateRequested = false;

  //   requestAnimationFrame(draw)
}
requestAnimationFrame(draw);
